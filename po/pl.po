# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Marcin Mikołajczak <me@mkljczk.pl>, 2017
# Marcin Mikołajczak <me@mkljczk.pl>, 2016
# No Ne, 2017-2018
# Piotr Sokół <psokol.l10n@gmail.com>, 2010
# Piotr Sokół <psokol.l10n@gmail.com>, 2017
# Piotr Strębski <strebski@gmail.com>, 2013
# Tomasz Chudyk <chudyk@gmail.com>, 2010
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-01 00:32+0200\n"
"PO-Revision-Date: 2019-07-31 22:32+0000\n"
"Last-Translator: Grzegorz Gębczyński <grzegorz.gebczynski@gmail.com>\n"
"Language-Team: Polish (http://www.transifex.com/xfce/xfce-apps/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: ../common/xfce-notify-log.c:362
msgid "Do you really want to clear the notification log?"
msgstr "Czy na pewno chcesz wyczyścić dziennik powiadomień?"

#: ../common/xfce-notify-log.c:366
msgid "Clear notification log"
msgstr "Wyczyść dziennik powiadomień"

#: ../common/xfce-notify-log.c:369
msgid "Cancel"
msgstr "Anuluj"

#: ../common/xfce-notify-log.c:371 ../xfce4-notifyd-config/main.c:957
msgid "Clear"
msgstr "Wyczyść"

#: ../common/xfce-notify-log.c:384
msgid "include icon cache"
msgstr "zawieraj pamięć podręczną ikon"

#: ../panel-plugin/notification-plugin.c:217
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:12
#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:1
msgid "Notifications"
msgstr "Powiadomienia"

#: ../panel-plugin/notification-plugin-dialogs.c:52
#, c-format
msgid "Unable to open the following url: %s"
msgstr "Nie można otworzyć następującego adresu url: %s"

#. create the dialog
#: ../panel-plugin/notification-plugin-dialogs.c:77
msgid "Notification Plugin Settings"
msgstr "Ustawienia wtyczki powiadomień"

#: ../panel-plugin/notification-plugin-dialogs.c:99
msgid "Number of notifications to show"
msgstr "Liczba powiadomień do wyświetlenia"

#: ../panel-plugin/notification-plugin-dialogs.c:111
msgid "Only show notifications from today"
msgstr "Pokaż powiadomienia tylko z dzisiaj"

#: ../panel-plugin/notification-plugin-dialogs.c:148
msgid "This is the notification plugin"
msgstr "To jest wtyczka powiadomień"

#: ../panel-plugin/notification-plugin-dialogs.c:150
msgid "Copyright © 2017 Simon Steinbeiß\n"
msgstr "Copyright © 2017 Simon Steinbeiß\n"

#: ../panel-plugin/notification-plugin-log.c:132
msgid "<b>_Do not disturb</b>"
msgstr "<b>_Nie przeszkadzać</b>"

#: ../panel-plugin/notification-plugin-log.c:318
msgid "No notifications"
msgstr "Brak powiadomień"

#: ../panel-plugin/notification-plugin-log.c:339
msgid "_Clear log"
msgstr "_Wyczyść dziennik"

#: ../panel-plugin/notification-plugin-log.c:347
msgid "_Notification settings…"
msgstr "_Ustawienia powiadomień..."

#: ../panel-plugin/notification-plugin.desktop.in.h:1
msgid "Notification Plugin"
msgstr "Wtyczka powiadomień"

#: ../panel-plugin/notification-plugin.desktop.in.h:2
msgid "Notification plugin for the Xfce panel"
msgstr "Wtyczka powiadomień dla panelu Xfce"

#: ../xfce4-notifyd/main.c:53 ../xfce4-notifyd/main.c:63
#: ../xfce4-notifyd-config/main.c:805
msgid "Xfce Notify Daemon"
msgstr "Demon powiadomień Xfce"

#: ../xfce4-notifyd/main.c:56
#, c-format
msgid "Unknown option \"%s\"\n"
msgstr "Nieznana opcja „%s”\n"

#: ../xfce4-notifyd/main.c:65
msgid "Unable to start notification daemon"
msgstr "Nie można uruchomić demona powiadomień"

#: ../xfce4-notifyd/xfce-notify-daemon.c:404
#, c-format
msgid "Another notification daemon is running, exiting\n"
msgstr "Uruchomiono już innego demona powiadomień, wyłączanie\n"

#: ../xfce4-notifyd-config/main.c:78
msgid "Notification Preview"
msgstr "Podgląd powiadomienia"

#: ../xfce4-notifyd-config/main.c:79
msgid "This is what notifications will look like"
msgstr "Tak będą wyglądać powiadomienia"

#: ../xfce4-notifyd-config/main.c:84
msgid "Button"
msgstr "Przycisk"

#: ../xfce4-notifyd-config/main.c:91
msgid "Notification preview failed"
msgstr "Nie udało się wyświetlić podglądu powiadomienia"

#: ../xfce4-notifyd-config/main.c:505
#, c-format
msgid ""
"<b>Currently only urgent notifications are shown.</b>\n"
"Notification logging is %s."
msgstr "<b>Obecnie wyświetlane są tylko pilne powiadomienia.</b>\nRejestrowanie powiadomień jest %s."

#: ../xfce4-notifyd-config/main.c:512
msgid "enabled"
msgstr "włączone"

#: ../xfce4-notifyd-config/main.c:512
msgid "disabled"
msgstr "wyłączone"

#: ../xfce4-notifyd-config/main.c:589
msgid "Yesterday and before"
msgstr "Wczoraj i wcześniej"

#: ../xfce4-notifyd-config/main.c:807
msgid "Settings daemon is unavailable"
msgstr "Demon menedżera ustawień jest niedostępny"

#: ../xfce4-notifyd-config/main.c:893
msgid ""
"<big><b>Currently there are no known applications.</b></big>\n"
"As soon as an application sends a notification\n"
"it will appear in this list."
msgstr "<big><b>Obecnie nie ma znanych aplikacji.</b></big>\nJak tylko aplikacja wyśle powiadomienie,\npojawi się na tej liście."

#: ../xfce4-notifyd-config/main.c:935
msgid ""
"<big><b>Empty log</b></big>\n"
"No notifications have been logged yet."
msgstr "<big><b>Pusty dziennik</b></big>\nŻadne powiadomienia nie zostały jeszcze zapisane w dzienniku."

#: ../xfce4-notifyd-config/main.c:943
msgid "Refresh"
msgstr "Wczytaj ponownie"

#: ../xfce4-notifyd-config/main.c:944
msgid "Refresh the notification log"
msgstr "Wczytaj ponownie dziennik powiadomień"

#: ../xfce4-notifyd-config/main.c:950
msgid "Open"
msgstr "Otwórz"

#: ../xfce4-notifyd-config/main.c:951
msgid "Open the notification log in an external editor"
msgstr "Otwórz dziennik powiadomień w zewnętrznym edytorze"

#: ../xfce4-notifyd-config/main.c:958
msgid "Clear the notification log"
msgstr "Wyczyść dziennik powiadomień"

#: ../xfce4-notifyd-config/main.c:980
msgid "Display version information"
msgstr "Wyświetla informacje o wersji"

#: ../xfce4-notifyd-config/main.c:981
msgid "Settings manager socket"
msgstr "Określa gniazdo menedżera ustawień"

#: ../xfce4-notifyd-config/main.c:981
msgid "SOCKET_ID"
msgstr "ID_GNIAZDA"

#: ../xfce4-notifyd-config/main.c:991
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Proszę wprowadzić „%s --help”, aby wypisać komunikat pomocy."

#: ../xfce4-notifyd-config/main.c:1006
#, c-format
msgid "Released under the terms of the GNU General Public License, version 2\n"
msgstr "Wydano na warunkach GNU General Public License, wersja 2\n"

#: ../xfce4-notifyd-config/main.c:1007
#, c-format
msgid "Please report bugs to %s.\n"
msgstr "Proszę zgłaszać błędy na adres %s.\n"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:1
msgid "only during \"Do not disturb\""
msgstr "tylko w trybie \"Nie przeszkadzaj\""

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:2
msgid "always"
msgstr "zawsze"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:3
msgid "all"
msgstr "wszystko"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:4
msgid "all except blocked"
msgstr "wszystkie poza zablokowanymi"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:5
msgid "only blocked"
msgstr "tylko zablokowane"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:6
msgid "display with mouse pointer"
msgstr "ekran ze wskaźnikiem myszy"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:7
msgid "primary display"
msgstr "główny ekran"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:8
msgid "Top left"
msgstr "Lewy górny narożnik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:9
msgid "Bottom left"
msgstr "Lewy dolny narożnik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:10
msgid "Top right"
msgstr "Prawy górny narożnik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:11
msgid "Bottom right"
msgstr "Prawy dolny narożnik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:13
msgid "Customize your notification settings"
msgstr "Dostosuj swoje ustawienia powiadomień"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:14
msgid "_Help"
msgstr "Pomo_c"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:15
msgid "_Close"
msgstr "_Zamknij"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:16
msgid ""
"The notification service is not running. No notifications will be shown."
msgstr "Usługa powiadomień nie jest uruchomiona. Nie będą wyświetlane żadne powiadomienia."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:17
msgid "<b>Currently only urgent notifications are shown.</b>"
msgstr "<b>Aktualnie wyświetlane są tylko pilne powiadomienia.</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:18
msgid "Show _Preview"
msgstr "Wyświetl p_odgląd"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:19
msgid "seconds"
msgstr "sekund"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:20
msgid "_Disappear after"
msgstr "_Znikaj po"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:21
msgid "_Opacity"
msgstr "Wid_oczność"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:22
msgid "Fade out"
msgstr "Wygaszanie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:23
msgid "_Slide out"
msgstr "_Wysuń"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:24
msgid "Default _position"
msgstr "Domyślne _położenie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:25
msgid "_Theme"
msgstr "_Styl"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:26
msgid "Do not disturb"
msgstr "Nie przeszkadzać"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:27
msgid "<b>Behavior</b>"
msgstr "<b>Zachowanie</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:28
msgid "<b>Appearance</b>"
msgstr "<b>Wygląd</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:29
msgid "<b>Animations</b>"
msgstr "<b>Animacje</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:30
msgid ""
"By default the notification bubbles will be shown on the display on which "
"the mouse pointer is located."
msgstr "Domyślnie, okna powiadomień są wyświetlane na tym ekranie, na którym znajduje się wskaźnik myszy."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:31
msgid "Show notifications on"
msgstr "Włączone wyświetlanie powiadomień"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:32
msgid "General"
msgstr "Ogólne"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:33
msgid "<b>Show or block notifications per application</b>"
msgstr "<b>Pokazuj lub blokuj powiadomienia dla aplikacji</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:34
msgid "Applications"
msgstr "Aplikacje"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:35
msgid "Log notifications"
msgstr "Zachowuj powiadomienia"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:36
msgid "Log applications"
msgstr "Zachowuj z aplikacji"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:37
msgid "Log"
msgstr "Dziennik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:2
msgid "Customize how notifications appear on your screen"
msgstr "Konfiguruje ustwienia powiadamiania"

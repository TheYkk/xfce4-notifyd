# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Carles Muñoz Gorriz <carlesmu@internautas.org>, 2008
# Davidmp <medipas@gmail.com>, 2016,2019
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016-2020
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-06 00:47+0200\n"
"PO-Revision-Date: 2020-05-06 11:03+0000\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan (http://www.transifex.com/xfce/xfce-apps/language/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../common/xfce-notify-log.c:385
msgid "Do you really want to clear the notification log?"
msgstr "Realment voleu netejar el registre de notificacions?"

#: ../common/xfce-notify-log.c:389
msgid "Clear notification log"
msgstr "Neteja el registre de notificacions"

#: ../common/xfce-notify-log.c:392
msgid "Cancel"
msgstr "Cancel·la"

#: ../common/xfce-notify-log.c:394 ../xfce4-notifyd-config/main.c:960
msgid "Clear"
msgstr "Neteja"

#: ../common/xfce-notify-log.c:407
msgid "include icon cache"
msgstr "inclou la memòria cau d'icones"

#: ../panel-plugin/notification-plugin.c:225
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:12
#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:1
msgid "Notifications"
msgstr "Notificacions"

#: ../panel-plugin/notification-plugin-dialogs.c:52
#, c-format
msgid "Unable to open the following url: %s"
msgstr "No es pot obrir el següent URL: %s"

#. create the dialog
#: ../panel-plugin/notification-plugin-dialogs.c:77
msgid "Notification Plugin Settings"
msgstr "Ajusts del connector de notificacions"

#: ../panel-plugin/notification-plugin-dialogs.c:80
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:14
msgid "_Help"
msgstr "_Ajuda"

#: ../panel-plugin/notification-plugin-dialogs.c:81
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:15
msgid "_Close"
msgstr "Tan_ca"

#: ../panel-plugin/notification-plugin-dialogs.c:99
msgid "Number of notifications to show"
msgstr "Nombre de notificacions a mostrar"

#: ../panel-plugin/notification-plugin-dialogs.c:111
msgid "Only show notifications from today"
msgstr "Mostra només les notificacions d'avui"

#: ../panel-plugin/notification-plugin-dialogs.c:148
msgid "This is the notification plugin"
msgstr "Aquest és un connector de notificació"

#: ../panel-plugin/notification-plugin-dialogs.c:150
msgid "Copyright © 2017 Simon Steinbeiß\n"
msgstr "Drets d'autor © 2017 Simon Steinbeiß\n"

#: ../panel-plugin/notification-plugin-log.c:132
msgid "<b>_Do not disturb</b>"
msgstr "<b>_No molestar</b>"

#: ../panel-plugin/notification-plugin-log.c:318
msgid "No notifications"
msgstr "Sense notificacions"

#: ../panel-plugin/notification-plugin-log.c:339
msgid "_Clear log"
msgstr "_Neteja el registre"

#: ../panel-plugin/notification-plugin-log.c:347
msgid "_Notification settings…"
msgstr "Ajusts de les _notificacions..."

#: ../panel-plugin/notification-plugin.desktop.in.h:1
msgid "Notification Plugin"
msgstr "Connector de notificacions"

#: ../panel-plugin/notification-plugin.desktop.in.h:2
msgid "Notification plugin for the Xfce panel"
msgstr "Connector de notificacions per al tauler de Xfce"

#: ../xfce4-notifyd/main.c:53 ../xfce4-notifyd/main.c:63
#: ../xfce4-notifyd-config/main.c:805
msgid "Xfce Notify Daemon"
msgstr "Dimoni de notificacions de Xfce"

#: ../xfce4-notifyd/main.c:56
#, c-format
msgid "Unknown option \"%s\"\n"
msgstr "Opció desconeguda: «%s»\n"

#: ../xfce4-notifyd/main.c:65
msgid "Unable to start notification daemon"
msgstr "No es pot iniciar el dimoni de notificacions"

#: ../xfce4-notifyd/xfce-notify-daemon.c:405
#, c-format
msgid "Another notification daemon is running, exiting\n"
msgstr "Ja hi ha un altre dimoni de notificacions executant-se, se surt\n"

#: ../xfce4-notifyd-config/main.c:78
msgid "Notification Preview"
msgstr "Vista prèvia de la notificació"

#: ../xfce4-notifyd-config/main.c:79
msgid "This is what notifications will look like"
msgstr "Així és com es veuran les notificacions"

#: ../xfce4-notifyd-config/main.c:84
msgid "Button"
msgstr "Botó"

#: ../xfce4-notifyd-config/main.c:91
msgid "Notification preview failed"
msgstr "Ha fallat la vista prèvia de la notificació"

#: ../xfce4-notifyd-config/main.c:505
#, c-format
msgid ""
"<b>Currently only urgent notifications are shown.</b>\n"
"Notification logging is %s."
msgstr "<b>Actualment només es mostren notificacions urgents.</b>\nEl registre de notificacions està %s."

#: ../xfce4-notifyd-config/main.c:512
msgid "enabled"
msgstr "habilitat"

#: ../xfce4-notifyd-config/main.c:512
msgid "disabled"
msgstr "inhabilitat"

#: ../xfce4-notifyd-config/main.c:589
msgid "Yesterday and before"
msgstr "Ahir i abans"

#: ../xfce4-notifyd-config/main.c:807
msgid "Settings daemon is unavailable"
msgstr "El dimoni d'ajusts no està disponible"

#: ../xfce4-notifyd-config/main.c:893
msgid ""
"<big><b>Currently there are no known applications.</b></big>\n"
"As soon as an application sends a notification\n"
"it will appear in this list."
msgstr "<big><b>Actualment no hi ha aplicacions conegudes.</b></big>\nTan aviat com una aplicació enviï una notificació\napareixerà en aquesta llista."

#: ../xfce4-notifyd-config/main.c:938
msgid ""
"<big><b>Empty log</b></big>\n"
"No notifications have been logged yet."
msgstr "<big><b>Registre buit</b></big>\nEncara no s'ha registrat cap notificació."

#: ../xfce4-notifyd-config/main.c:946
msgid "Refresh"
msgstr "Refresca"

#: ../xfce4-notifyd-config/main.c:947
msgid "Refresh the notification log"
msgstr "Refresca el registre de notificacions"

#: ../xfce4-notifyd-config/main.c:953
msgid "Open"
msgstr "Obre"

#: ../xfce4-notifyd-config/main.c:954
msgid "Open the notification log in an external editor"
msgstr "Obre el registre de notificacions en un editor extern"

#: ../xfce4-notifyd-config/main.c:961
msgid "Clear the notification log"
msgstr "Neteja el registre de notificacions"

#: ../xfce4-notifyd-config/main.c:983
msgid "Display version information"
msgstr "Mostra la informació de la versió"

#: ../xfce4-notifyd-config/main.c:984
msgid "Settings manager socket"
msgstr "Sòcol del gestor d'ajusts"

#: ../xfce4-notifyd-config/main.c:984
msgid "SOCKET_ID"
msgstr "SOCKET_ID"

#: ../xfce4-notifyd-config/main.c:994
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Teclegeu «%s --help» per a l'ús."

#: ../xfce4-notifyd-config/main.c:1009
#, c-format
msgid "Released under the terms of the GNU General Public License, version 2\n"
msgstr "Publicat sota els termes de la «GNU General Public License», versió 2\n"

#: ../xfce4-notifyd-config/main.c:1010
#, c-format
msgid "Please report bugs to %s.\n"
msgstr "Informeu els errors a %s.\n"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:1
msgid "only during \"Do not disturb\""
msgstr "només amb «No molestar»"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:2
msgid "always"
msgstr "sempre"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:3
msgid "all"
msgstr "totes"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:4
msgid "all except blocked"
msgstr "totes excepte les bloquejades"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:5
msgid "only blocked"
msgstr "només les bloquejades"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:6
msgid "display with mouse pointer"
msgstr "pantalla amb el punter del ratolí"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:7
msgid "primary display"
msgstr "pantalla primària"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:8
msgid "Top left"
msgstr "Part superior esquerra"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:9
msgid "Bottom left"
msgstr "Part inferior esquerra"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:10
msgid "Top right"
msgstr "Part superior dreta"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:11
msgid "Bottom right"
msgstr "Part inferior dreta"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:13
msgid "Customize your notification settings"
msgstr "Personalitzeu els ajusts de les vostres notificacions"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:16
msgid ""
"The notification service is not running. No notifications will be shown."
msgstr "No s'està executant el servei de les notificacions. No es mostraran les notificacions."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:17
msgid "<b>Currently only urgent notifications are shown.</b>"
msgstr "<b>Actualment només es mostren les notificacions urgents.</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:18
msgid "Show _Preview"
msgstr "Mostra la vista _prèvia"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:19
msgid "seconds"
msgstr "segons"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:20
msgid "_Disappear after"
msgstr "_Desapareix després de"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:21
msgid "_Opacity"
msgstr "_Opacitat"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:22
msgid "Fade out"
msgstr "Esvaïment"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:23
msgid "_Slide out"
msgstr "Lli_sca cap a fora"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:24
msgid "Default _position"
msgstr "_Posició predeterminada"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:25
msgid "_Theme"
msgstr "_Tema"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:26
msgid "Do not disturb"
msgstr "No molestar"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:27
msgid "<b>Behavior</b>"
msgstr "<b>Comportament</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:28
msgid "<b>Appearance</b>"
msgstr "<b>Aparença</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:29
msgid "<b>Animations</b>"
msgstr "<b>Animacions</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:30
msgid ""
"By default the notification bubbles will be shown on the display on which "
"the mouse pointer is located."
msgstr "Per defecte, les bombolles de notificació es mostraran a la pantalla que es troba el punter del ratolí."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:31
msgid "Show notifications on"
msgstr "Mostra les notificacions a la"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:32
msgid "General"
msgstr "General"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:33
msgid "<b>Show or block notifications per application</b>"
msgstr "<b>Mostra o bloqueja les notificacions en funció de l'aplicació</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:34
msgid "Applications"
msgstr "Aplicacions"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:35
msgid "Log notifications"
msgstr "Enregistrament de les notificacions"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:36
msgid "Log applications"
msgstr "Enregistrament de les aplicacions"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:37
msgid "Log size limit"
msgstr "Mida màxima del registre"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:38
msgid ""
"The maximum amount of entries to be retained in the log. Please note that a "
"large log may lead to performance penalties. 0 means no limit."
msgstr "El nombre màxim d’entrades a conservar al registre. Tingueu en compte que un registre gran pot comportar penalitzacions en el rendiment. 0 significa que no hi ha cap límit."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:39
msgid "Log"
msgstr "Registre"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:2
msgid "Customize how notifications appear on your screen"
msgstr "Personalitzeu com es mostren les notificacions"

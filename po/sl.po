# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aljoša Žagar <anthon.manix@gmail.com>, 2013
# Arnold Marko <arnold.marko@gmail.com>, 2020
# Kernc, 2015
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-01 00:32+0200\n"
"PO-Revision-Date: 2020-02-13 09:13+0000\n"
"Last-Translator: Arnold Marko <arnold.marko@gmail.com>\n"
"Language-Team: Slovenian (http://www.transifex.com/xfce/xfce-apps/language/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: ../common/xfce-notify-log.c:362
msgid "Do you really want to clear the notification log?"
msgstr "Bi res radi počistili dnevnik obvestil?"

#: ../common/xfce-notify-log.c:366
msgid "Clear notification log"
msgstr "Počisti dnevnik obvestil"

#: ../common/xfce-notify-log.c:369
msgid "Cancel"
msgstr "Prekliči"

#: ../common/xfce-notify-log.c:371 ../xfce4-notifyd-config/main.c:957
msgid "Clear"
msgstr "Počisti"

#: ../common/xfce-notify-log.c:384
msgid "include icon cache"
msgstr "vključi medpomnilnik za ikone"

#: ../panel-plugin/notification-plugin.c:217
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:12
#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:1
msgid "Notifications"
msgstr "Obvestila"

#: ../panel-plugin/notification-plugin-dialogs.c:52
#, c-format
msgid "Unable to open the following url: %s"
msgstr "Ne morem odpreti naslednje url povezave: %s"

#. create the dialog
#: ../panel-plugin/notification-plugin-dialogs.c:77
msgid "Notification Plugin Settings"
msgstr "Nastavitve vtičnika za obvestila"

#: ../panel-plugin/notification-plugin-dialogs.c:99
msgid "Number of notifications to show"
msgstr "Število prikazanih obvestil"

#: ../panel-plugin/notification-plugin-dialogs.c:111
msgid "Only show notifications from today"
msgstr "Prikaži le današnja obvestila"

#: ../panel-plugin/notification-plugin-dialogs.c:148
msgid "This is the notification plugin"
msgstr "To je vtičnik za obvestila"

#: ../panel-plugin/notification-plugin-dialogs.c:150
msgid "Copyright © 2017 Simon Steinbeiß\n"
msgstr "© 2017 Simon Steinbeiß\n"

#: ../panel-plugin/notification-plugin-log.c:132
msgid "<b>_Do not disturb</b>"
msgstr "<b>Ne _moti</b>"

#: ../panel-plugin/notification-plugin-log.c:318
msgid "No notifications"
msgstr "Brez obvestil"

#: ../panel-plugin/notification-plugin-log.c:339
msgid "_Clear log"
msgstr "Po_briši dnevnik"

#: ../panel-plugin/notification-plugin-log.c:347
msgid "_Notification settings…"
msgstr "_Nastavitve obveščanja..."

#: ../panel-plugin/notification-plugin.desktop.in.h:1
msgid "Notification Plugin"
msgstr "Vtičnik za obvestila"

#: ../panel-plugin/notification-plugin.desktop.in.h:2
msgid "Notification plugin for the Xfce panel"
msgstr "Vtičnik za obvestila za Xfce pano"

#: ../xfce4-notifyd/main.c:53 ../xfce4-notifyd/main.c:63
#: ../xfce4-notifyd-config/main.c:805
msgid "Xfce Notify Daemon"
msgstr "Xfce pritajeni program za obvestila"

#: ../xfce4-notifyd/main.c:56
#, c-format
msgid "Unknown option \"%s\"\n"
msgstr "Neznana možnost \"%s\"\n"

#: ../xfce4-notifyd/main.c:65
msgid "Unable to start notification daemon"
msgstr "Pritajenega programa za obvestila ni mogoče zagnati"

#: ../xfce4-notifyd/xfce-notify-daemon.c:404
#, c-format
msgid "Another notification daemon is running, exiting\n"
msgstr "Zagnan je še en pritajeni program za obvestila. Prekinjam.\n"

#: ../xfce4-notifyd-config/main.c:78
msgid "Notification Preview"
msgstr "Predogled obvestila"

#: ../xfce4-notifyd-config/main.c:79
msgid "This is what notifications will look like"
msgstr "Tako bodo prikazana obvestila"

#: ../xfce4-notifyd-config/main.c:84
msgid "Button"
msgstr "Gumb"

#: ../xfce4-notifyd-config/main.c:91
msgid "Notification preview failed"
msgstr "Predogled obvestil ni uspel"

#: ../xfce4-notifyd-config/main.c:505
#, c-format
msgid ""
"<b>Currently only urgent notifications are shown.</b>\n"
"Notification logging is %s."
msgstr "<b>Trenutno so prikazana le nujna obvestila.</b>\nBeleženje obvestil v dnevnik je %s"

#: ../xfce4-notifyd-config/main.c:512
msgid "enabled"
msgstr "vklopljeno"

#: ../xfce4-notifyd-config/main.c:512
msgid "disabled"
msgstr "izklopljeno"

#: ../xfce4-notifyd-config/main.c:589
msgid "Yesterday and before"
msgstr "Včeraj in pred tem"

#: ../xfce4-notifyd-config/main.c:807
msgid "Settings daemon is unavailable"
msgstr "Pritajeni program za nastavitve ni na voljo"

#: ../xfce4-notifyd-config/main.c:893
msgid ""
"<big><b>Currently there are no known applications.</b></big>\n"
"As soon as an application sends a notification\n"
"it will appear in this list."
msgstr "<big><b>Trenutno ni znanih programov.</b></big>\nTakoj, ko bo program poslalo obvestilo,\nse bo to pojavilo v seznamu."

#: ../xfce4-notifyd-config/main.c:935
msgid ""
"<big><b>Empty log</b></big>\n"
"No notifications have been logged yet."
msgstr "<big><b>Prazen dnevnik</b></big>\nNi zabeleženih obvestil."

#: ../xfce4-notifyd-config/main.c:943
msgid "Refresh"
msgstr "Osveži"

#: ../xfce4-notifyd-config/main.c:944
msgid "Refresh the notification log"
msgstr "Osveži dnevnik obvestil"

#: ../xfce4-notifyd-config/main.c:950
msgid "Open"
msgstr "Odpri"

#: ../xfce4-notifyd-config/main.c:951
msgid "Open the notification log in an external editor"
msgstr "Odpri dnevnik obvestil v zunanjem urejevalniku"

#: ../xfce4-notifyd-config/main.c:958
msgid "Clear the notification log"
msgstr "Počisti dnevnik obvestil"

#: ../xfce4-notifyd-config/main.c:980
msgid "Display version information"
msgstr "Pokaži podatke o različici"

#: ../xfce4-notifyd-config/main.c:981
msgid "Settings manager socket"
msgstr "Vtičnik za upravljalnje nastavitev"

#: ../xfce4-notifyd-config/main.c:981
msgid "SOCKET_ID"
msgstr "SOCKET_ID"

#: ../xfce4-notifyd-config/main.c:991
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Vpišite terminalski ukaz '%s --help' za navodila za uporabo."

#: ../xfce4-notifyd-config/main.c:1006
#, c-format
msgid "Released under the terms of the GNU General Public License, version 2\n"
msgstr "Izdano pod pogoji splošnega dovoljenja GNU, različica 2\n"

#: ../xfce4-notifyd-config/main.c:1007
#, c-format
msgid "Please report bugs to %s.\n"
msgstr "Poročajte hrošče na %s.\n"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:1
msgid "only during \"Do not disturb\""
msgstr "zoglj kadar je izbrano 'Ne moti'"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:2
msgid "always"
msgstr "vedno"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:3
msgid "all"
msgstr "vse"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:4
msgid "all except blocked"
msgstr "vse razen blokiranih"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:5
msgid "only blocked"
msgstr "zgolj blokirane"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:6
msgid "display with mouse pointer"
msgstr "prikaz s kazalcem miške"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:7
msgid "primary display"
msgstr "glavni zaslon"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:8
msgid "Top left"
msgstr "Zgoraj levo"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:9
msgid "Bottom left"
msgstr "Spodaj levo"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:10
msgid "Top right"
msgstr "Zgoraj desno"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:11
msgid "Bottom right"
msgstr "Spodaj desno"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:13
msgid "Customize your notification settings"
msgstr "Prilagajanje nastavitev obveščanja"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:14
msgid "_Help"
msgstr "_Pomoč"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:15
msgid "_Close"
msgstr "_Zapri"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:16
msgid ""
"The notification service is not running. No notifications will be shown."
msgstr "Storitev obveščanja se ne izvaja. Obvestila ne bodo prikazana."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:17
msgid "<b>Currently only urgent notifications are shown.</b>"
msgstr "<b>Trenutno se prikazujejo zgolj nujna opozorila.</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:18
msgid "Show _Preview"
msgstr "Prikaži _predogled"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:19
msgid "seconds"
msgstr "sekund"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:20
msgid "_Disappear after"
msgstr "Izgine _čez "

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:21
msgid "_Opacity"
msgstr "Nepr_osojnost"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:22
msgid "Fade out"
msgstr "Mehko izginevanje"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:23
msgid "_Slide out"
msgstr "Premakni _ven"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:24
msgid "Default _position"
msgstr "Privzeti _položaj"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:25
msgid "_Theme"
msgstr "_Tema"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:26
msgid "Do not disturb"
msgstr "Ne moti"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:27
msgid "<b>Behavior</b>"
msgstr "<b>Obnašanje</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:28
msgid "<b>Appearance</b>"
msgstr "<b>Pojavljanje</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:29
msgid "<b>Animations</b>"
msgstr "<b>Animacije</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:30
msgid ""
"By default the notification bubbles will be shown on the display on which "
"the mouse pointer is located."
msgstr "Privzeto je, da je oblaki z obvestili pojavijo na zaslonu, na keterem se nahaja kazalec miške."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:31
msgid "Show notifications on"
msgstr "Prikaži obestila na"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:32
msgid "General"
msgstr "Splošno"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:33
msgid "<b>Show or block notifications per application</b>"
msgstr "<b>Prikaži ali blokiraj obestila za program</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:34
msgid "Applications"
msgstr "Aplikacije"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:35
msgid "Log notifications"
msgstr "Beleži obvestila"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:36
msgid "Log applications"
msgstr "Beleži programe"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:37
msgid "Log"
msgstr "Dnevnik"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:2
msgid "Customize how notifications appear on your screen"
msgstr "Prilagodite prikaz obvestil na zaslonu"
